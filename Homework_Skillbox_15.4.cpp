// Homework_Skillbox_15.4.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>

void PrintNumbers(int N, bool isEven)
 {
    //prints numbers from 0 to N, depending on the bool parameter
    for (int i = 0; i <= N; ++i)
    {
        bool OddOrEven = ((i % 2 == isEven) && (i != 0));
        switch (OddOrEven)
        {
        case true:
            std::cout << i << " ";
            break;
        case false:
            break;
        }
    }
    std::cout << "\n";
 }
 
int main()
 {
     PrintNumbers(100, true); //Even
     PrintNumbers(100, false); //Odd
 }




/*

// my first variant where function and program in same body
// and with user input

int Limit;
bool UserChoice;

int main()
{
    std::cout << "Enter limit of numbers\n";
    std::cin >> Limit;
    std::cout << "Enter ""0"" to print Odd values or ""1"" to print Even values:\n";
    std::cin >> UserChoice;
    for (int i = 0; i <= Limit; ++i)
    {
        bool OddOrEven = ((i % 2 == UserChoice) && (i != 0));
        switch (OddOrEven)
        {
        case true:
            std::cout << i << " ";
            break;
        case false:
            break;
        }
    }
    std::cout << "\n\n";
}

*/
